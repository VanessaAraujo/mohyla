﻿using UnityEngine;
using System.Collections;


/// <summary>
/// THIS SCRIPT CONTROLS THE CAMERA MOVEMENT FOR ROTATING AND MOVING TOWARDS PLAYER, AIMING AND PAUSE POSITION
/// </summary>
public class CameraFollow : MonoBehaviour {


	[SerializeField]
	private float hDistance; //horizontal and vertical distance to the player are with atribute serializedfield so the can appear in the adito but still be private variables.
	[SerializeField]
	private float vDistance;

	public bool follow = true; // flags if the camera needs to   follow to the player tracker position
	public bool look = true; // flags the if the camera needs to set its rotation
	public bool aim;
	public bool isAiming;

	public float cameraDamp; // the damp or speed to follow the track
	public Transform leftHandAim; 
	public Transform player;

	public float lookAngle;
	public float tiltAngle;
	private const float LookDistance = 100.0f;
	private LayerMask layerMaskWallCollision;


	private Vector3 targetPosition;
	private Transform playerTrackTransform;

	void Start () {
		
		layerMaskWallCollision = 1 << 8;
		layerMaskWallCollision = ~layerMaskWallCollision;
		playerTrackTransform = GameObject.FindWithTag ("test").transform; // find the game object parented to the player

	}
		
	void LateUpdate () {
		//Debug.Log (playerTrackTransform.tag);
		
		if (follow) { // follow is false when in aim state or during pause
			targetPosition = playerTrackTransform.position + playerTrackTransform.up * vDistance - playerTrackTransform.forward * hDistance; // target position is directed to player head hight
			RaycastHit wallHit;
			if (Physics.Raycast (targetPosition, playerTrackTransform.position - targetPosition, out wallHit, Mathf.Infinity,10)) {
				targetPosition = new Vector3 (wallHit.point.x, wallHit.point.y, wallHit.point.z);
			}
			transform.position = Vector3.Lerp (transform.position, targetPosition, cameraDamp * Time.deltaTime); // lerps camera position to the target position keepping distance hDistance and vDistance from player
		}
			
		if (Input.GetMouseButtonDown (0)) { // if aim rotate player and the reference to adjust to the animation rotation and the position of the player on the screen during aiming
			player.Rotate (0, 60.0f, 0);
			playerTrackTransform.Rotate (0, -90.0f, 0);

		}

		if (Input.GetMouseButtonUp (0)) {
			player.Rotate (0, -60.0f, 0);
			playerTrackTransform.Rotate (0, 90.0f, 0);

		}
			
		if (look && follow) // rotates towards the player
			transform.LookAt (playerTrackTransform.position);

		if (look && !follow)//rotates towards the hand
			transform.LookAt (leftHandAim.position + leftHandAim.forward *0.3f);
	
	}
		
	public IEnumerator SwitchToAim(){ // start aiming state , coroutine called by the player script
		
		follow = false; 
		look = false;
		yield return new WaitForSeconds(1.0f); // stop following and wait for  equip the animation to finish
		transform.position = playerTrackTransform.position + playerTrackTransform.right * 0.6f - playerTrackTransform.forward * 0.2f ; // change camera position to the right of the player and closer
		look = true; // look true and follow false, look at the hand
	}	
		public IEnumerator SwitchFromAim(){
		
		transform.LookAt (player); 
		transform.position = playerTrackTransform.position + playerTrackTransform.up * vDistance - playerTrackTransform.forward * hDistance; // go back to the following position
		follow = true; // follows and rotates towardsv player 
		look = true;
		yield return new WaitForSeconds(1.0f);
	}	
		
	public IEnumerator LookDelay(){
		follow = false;
		look = false;
		yield return new WaitForSeconds(0.8f);
		look = true;
		
	}






}
