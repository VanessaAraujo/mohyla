﻿using UnityEngine;
using System.Collections;


/// <summary>
/// CONTROLS WHICH OF THE BOWS ARE VISIBLE
/// </summary>
public class BowManager : MonoBehaviour {


	public GameObject bowAim; // the position on the player left hand that holds the bow
	public GameObject bowBack; // the bow that is in the back of the player
	private SkinnedMeshRenderer bowAimSkinnedRenderer;

	void Start () {
		bowAimSkinnedRenderer = bowAim.GetComponentInChildren<SkinnedMeshRenderer> (); // the skinned renderer  of the aiming bow
	}


	void EquipBow() // the function is called by the equiping animation clip events system to unrender the bow in the back and render the one in the left hand
	{
		
		bowAimSkinnedRenderer.enabled = true;
		bowBack.SetActive(false);
	}

	void DisarmBow()
	{
		
		bowAimSkinnedRenderer.enabled = false;
		bowBack.SetActive(true);
	}
}
