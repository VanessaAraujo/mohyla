﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

/// <summary>
/// FOOTSTEP SPAWNING
/// </summary>
public class Footsteps : MonoBehaviour {


	public AudioSource rightFootstepClip; // audio sources for the footsteps attached to the characters feet
	public AudioSource leftFootstepClip;

	public GameObject rightFootPrint; //the prefab footprint, a quad with a texture and normal map
	public GameObject leftFootPrint;

	private Transform rightFoot; //this will store right and left feet transform
	private Transform leftFoot;
	private Animator anim; // the player's animator

	public Quaternion rotation;






	void Start()
	{
		anim = GetComponent<Animator> ();
		rightFoot = anim.GetBoneTransform (HumanBodyBones.RightFoot); // sks the position of the feet joint for the mecanim.
		leftFoot = anim.GetBoneTransform (HumanBodyBones.LeftFoot);
	}



	void RightFootStep() // this function is called by the events placed on each of the movement animation in the moment when the character touches the floor
	{
		
		RaycastHit hit;
		if(Physics.Raycast(rightFoot.position, -Vector3.up, out hit, 1.0f)) // if the raycasting from the foot position hits the Terrain, plays the audio sources and instantiate a footprint prefab 
		{	if(hit.transform.name=="Terrain" ){
				
				rightFootstepClip.Play ();


				rotation = Quaternion.FromToRotation (transform.up, hit.normal) * transform.localRotation; //calculates the rotation to go from the ground perpendicular to the perpendicular of the character transform , multiplying the result by the local rotation, to bring the real rotation in the word space

				rotation *= Quaternion.Euler(90, 0, 0); // rotates 90 degrees so the quad can be seen
				StartCoroutine(DelayFootstep(hit));
					

			}
		
		}
	}

	void LeftFootStep()
	{
		
		RaycastHit hit;
		if(Physics.Raycast(leftFoot.position, -Vector3.up, out hit, 1.0f))
		{	
			if(hit.transform.name=="Terrain"){
				leftFootstepClip.Play ();

				rotation = Quaternion.FromToRotation (transform.up, hit.normal) * transform.localRotation;
				rotation *= Quaternion.Euler(90, 0, 0);
				StartCoroutine(DelayFootstep(hit)); //- leftFoot.forward*0.08f, rotation);


			}
		}

	}

	IEnumerator DelayFootstep(RaycastHit hitArg){
		

		Instantiate (rightFootPrint, hitArg.point + hitArg.normal* 0.1f   - transform.forward*0.25f, rotation);
		yield return null;
	}


 


}
