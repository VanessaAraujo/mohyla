﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

/// <summary>
/// CONTROLS BOW ANIMATION STATES AND ARROW TRACKING AND SPAWNING SYSTEM
/// </summary>

public class BowControl : MonoBehaviour {

	public Animator anim;	//the bow animator
	public Camera myCamera;// main camera
	public CameraFollow cameraScript;// script attached to the camera
	public PlayerMovement playerScript;// script attached to the player
	public GameObject projectile;// the arrow prefab
	public Transform projectileParent;// // the arrow parent
	public int numberOfArrows =2;// number of arrows avaiable;
	public IKController IKScript;// IK script
	public Image crosshair;// the aiming crosshair
	public float force;		// the force to be added to the arrow

	private GameObject newProjectileInstance;// the gameobject will store the arrow instance
	private bool aim;// flags the aiming state
	private bool wasShot= true;// flags if last arrow was shot
	private Vector3 originalPosition;// the Transform of the arrow prefab, 
	private Vector3 originalLocalPosition;//
	private Vector3 originalEulerAngles;//
	private Vector3 originalScale;//
	private Quaternion originalRotation;//
	private List<GameObject> projectiles;// a lista to store created projectiles
	private LayerMask arrowTargetLayerMask;//
	private Animator animator;


	void Start () {

		arrowTargetLayerMask = 1 << 8;	// this filter everything but the layer 8 of the player
		arrowTargetLayerMask = ~arrowTargetLayerMask; // inverts the mask, filterinng only the layer 8
		crosshair.enabled = false; 
		projectiles = new List<GameObject>();

			if (projectile != null) 
			{
				projectile.GetComponent<MeshRenderer>().enabled = false; // if prefab in the scene, store the original prefab position
				originalEulerAngles = projectile.transform.localEulerAngles;
				originalLocalPosition = projectile.transform.localPosition;
				originalPosition = projectile.transform.position;
				originalScale = projectile.transform.localScale;
				originalRotation = projectile.transform.rotation;
			}

			animator = GetComponent<Animator>();	

		}


	void Reload () 
	{		if (aim) //if aiming, instantiate an arrow, sets its gravity to zero and its position to the original prefab arrow, make the arrow visible by enabling the render and add the information to a control list of arrows in the scene. 
			{
				
				newProjectileInstance = Instantiate (projectile, originalPosition, originalRotation) as GameObject;
				Rigidbody projectileRigidbody = newProjectileInstance.GetComponent<Rigidbody> ();
				if (projectileRigidbody != null) {
					projectileRigidbody.useGravity = false;
				}

				newProjectileInstance.transform.parent = projectileParent;
				newProjectileInstance.transform.localPosition = originalLocalPosition;
				newProjectileInstance.transform.localEulerAngles = originalEulerAngles;
				newProjectileInstance.transform.localScale = originalScale;

				newProjectileInstance.GetComponent<MeshRenderer> ().enabled = true;
			    projectiles.Insert (0, newProjectileInstance);
			}

	}

	void Update () {
			
		if (Input.GetMouseButtonDown(0)) // listens for the leftmouse button, if the player keeps pressing, the player goes to aiming state
		{
			aim = true;	
			wasShot = true; //wasShot shot is true untill we instantiate another arrow
			Load (); 
		}

		crosshair.enabled = aim; // the position off the crosshair image is set to the middle of the screen 
	
		if (Input.GetMouseButtonUp(0))  // If releases the leftmousebutton, the bow animator goes to the idle state on the animator
		{
			aim = false;
			anim.SetBool ("shoot",false);
			animator.SetBool ("Idle", true);
			animator.SetBool("Shoot", false);
			animator.SetBool("Load", false);
		
			Destroy (newProjectileInstance); // destroy the last arrow created
			if (projectiles.Count>0) 
			{
				projectiles.RemoveAt (0);
			}
				//}
			//}
				
		}

		Vector3 spawnDirection = myCamera.transform.forward;  
			if(Input.GetButtonDown("Fire2") && aim)// if the user fires while aiming, the camera stops to follow the hand for a while, while the upper body loading animation goes on
		{
			StartCoroutine (cameraScript.LookDelay());
			anim.SetBool ("shoot",true);
			animator.SetBool ("Idle", false);
			animator.SetBool("Shoot", true);// it plays the shoot animation for the bow
			animator.SetBool("Load", false);
			wasShot = true;
			Invoke ("Load", 0.5f); // load again
		
				if(projectiles.Count>0) // if there are still projetiles in the scene
			{
				Rigidbody projectileRigidbody = projectiles[0].GetComponent<Rigidbody>();
				BoxCollider projectileBoxCollider = projectiles[0].GetComponent<BoxCollider>();
				projectiles[0].transform.parent = null;

				if(projectileRigidbody!=null && projectileRigidbody.useGravity == false) 
				{
					projectileRigidbody.useGravity = true;
					projectileRigidbody.isKinematic = false;
					projectileBoxCollider.enabled = true;

					projectiles [0].transform.rotation = Quaternion.LookRotation (spawnDirection); // settles the rigidbody and collider properties and unparents the objext before it adds a force to the arrow
					projectiles [0].GetComponent<ArrowControl> ().flying = true;
					projectileRigidbody.AddForce(spawnDirection * force);
				
				}
			}
			numberOfArrows--; //after shooting, reduce the amount of arrows to the player and updates its script
			playerScript.arrow = numberOfArrows;
		}				
	} 

	void Load() // on load, it sets the animatoir to loas, only if the number of avaiable arrows are greater than zero, invoking reload
	{
				if (aim) {
					if (wasShot == true && numberOfArrows>0) {
					Invoke ("Reload", 0.4f);
					wasShot = false;
					}
					anim.SetBool ("shoot", false);
					animator.SetBool ("Idle", false);
					animator.SetBool ("Shoot", false);
					animator.SetBool ("Load", true);
				}

	}
		
		
}

