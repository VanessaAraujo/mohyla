﻿using UnityEngine;
using System.Collections;


/// <summary>
/// FADES AND DESTROY FOOTPRINT AFTER 5 SECONDS.
/// </summary>
public class DestroyFootprint : MonoBehaviour {


	private Renderer rend;


	void Start () {
	
		rend = GetComponent<Renderer> (); // the rendere attached to each footprint
		Destroy (gameObject, 5.0f); // destroys the gameobject in 5 seconds
	}

	void Update()

	{
		


			
		Color color = rend.material.color;
		float alpha = color.a;
		color.a = Mathf.Lerp (alpha, 0, 0.01f);
		rend.material.color = new Color (color.r, color.g, color.b, color.a); // smootyhly changes the alpha of the material


	}

}
