﻿using UnityEngine; // Unity Engine standard classes Library
using UnityEngine.SceneManagement; // This Library allow us to access the class SceneManager
using UnityEngine.UI; // This Library allow us to access all the UI game objects
using UnityEngine.Audio; // This Library allow us to reference the "AudioMixer" class and its methods, "SetFloat" and "GetFloat"
//using UnityStandardAssets.ImageEffects; //  This Library allow us to reference the "ColourCorrectionCurves" component, that is attached to the Main Camera, and is part of the Unity Image Effects Asset Package.
using System.IO; // This Library allows us to reference the "File" class and its methods: ".Exists", ".ReadAllText" and ".WriteAllText.
using System; // This Library allows us to use the [Serializable] atribute.
using System.Collections;



//THIS SCRIPT CONTROLS ALL THE UI MENU BEHAVIOURS. DURING GAMEPLAY, THE USER CAN ALSO PRESS SCAPE TO PAUSE THE GAME OR QUIT, SAVING ALL ITS PREFERENCES
//FOR THIS ASSIGNMENT WE HAVE USED THE LECTURE 6 AS A REFERENCE, AS WELL AS THE UNITY LEARN FORUM
//THE QUALITY SETTINGS ONLY WORK PROPERLY USING UNITY 5.4

[Serializable] // This atribute allows the class UserPreferences, to be Serialized, and be converted to Json format later on, by the JsonUtility class
public class UserPreferences // This class represents a User set of  settigns Preferences
{	// class properties:
	public string userName;
	public float userMasterVolume;
	public float userMusicVolume;
	public float userSfxVolume;
	public bool userIsMute;
	public int userGraphicsQuality;
	public int userResolutionIndex;
	public bool userIsFullScreen;

	public int energyCube;
	public int arrows;
}


public class MenuManager : MonoBehaviour { 
	
	public GameObject mainCam; // the "public" atribute exposes the class property mainCam of type GameObject in the inspector. Now we can assign a scene game object to that property. In that case The Main Camera.
	public CameraFollow cameraScript; // the script attached to the camera
	public GameObject myMainMenu; // The main Menu. myMainMenu references an empty game Object in the scene that parents all main menu UI elements
	public GameObject mySettingsMenu; // The Settings menu. mySettingsMenu references an empty game Object in the scene that parents all Settings Menu UI Elements
	public GameObject myNewGameMenu; // The New Game Menu. myNewGameMenu references an empty game Object in the scene that parents all the UI elements from the New Game Menu
	public GameObject myPauseMenu; // The Pause Menu. myPauseMenu references an empty game Object in the scene that parents all the UI elements from the pause menu 

	public Slider[] volumeSliders; // An array of UI elements of type Slider. The three volume sliders from the settings menu are assigned to that array in the inspector
	public Toggle[] resolutionToggles; // An Array of UI elements of type Toggle. The three resolution toggles from the settings menu are assigned to that array in the inspector
	public Toggle fullScreenToggle; // A toggle elements used to control the full screen behaviour. The Full Screen toggle in the Settings Menu is assigned to that Toggle
	public Toggle isMuteToggle; // A toggle elements used to control the mute volume behaviour. The mute toggle in the Settings Menu is assigned to that Toggle element
	public Dropdown graphicsQuality; // A dropdown object used to control the graphics quality. The dropdown element in the settings Menu is assigned to that object.
	public int[] screenWidths; // An array of integers containing the possible horizontal resolutions. This is assigned manually in the inspector, more resolutions can be added if needed.
	public AudioMixer myAudioMixer; // An Audio Mixer was created in the editor to control Master, music and Sfx individually. Their volumes were exposed as parameters. The audio souces in the Main Camera are assigned each one to a different channel of the Audio Mixer. The Audio Mixer created is assigned to this myAudioMixer element.

	public Text ouputMasterVolume; // Text UI element to output the currently master volume value in Db's (-80 to 0) in the Settings Menu.
	public Text ouputMusicVolume; // Text UI element to output the currently music volume value in Db's (-80 to 0) in the Settings Menu.
	public Text ouputSfxVolume; // Text UI element to output the currently sound effects volume value in Db's (-80 to 0) in the Settings Menu.
	public Text ouputSaturation; // Text UI element to output the currently saturation value in unities (0 to 5) in the Settings Menu.
	    
	bool isUserSliding0 = true; // boolean variable it is true when the Master Volume Slide value is being changed by the user (true) or by the VolumeMute() method (false).
	bool isUserSliding1 = true; // boolean variable it is true when the Music Volume Slide value is being changed by the user (true) or by the VolumeMute() method (false)
	bool isUserSliding2 = true; // boolean variable it is true when the Sfx Volume Slide value is being changed by the user (true) or by the VolumeMute() method (false)
	public bool paused = false; // boolean variable is true when the game is paused.
	public bool pressedPause; // a boolean to help on pause system
	private GameObject player; //
	private PlayerMovement playerScript;


	UserPreferences currentlyUserPreferences; // istance of the serialized class "UserPreferences". This will keep record of all user preferences properties during the runtime.
	string filePath; // string containing the path to the running application in the device or computer to a read and write directory Application.persistentDataPath. 
	const string FILE_NAME = "UserPreferences.json"; // a constant string with the name of the Json File that will keeo the user settings, on a Json format, even after the runtime.



	void Start() // Lifecycle runtime beggins. Code runs once when the scene starts to play

	{	if(GameObject.FindWithTag ("Player") !=null)
		playerScript = GameObject.FindWithTag ("Player").GetComponent<PlayerMovement> ();
		if (SceneManager.GetActiveScene ().buildIndex == 1) {
			myMainMenu.SetActive (false);
		}
		filePath = Application.persistentDataPath; // the path in the system to the Persistent Data directory in the running application folder is assigned to the filePath string
		Debug.Log(filePath);
		currentlyUserPreferences = new UserPreferences (); // the new object currentlyUserPreferences is an instance of the UserPreferences class. It will keep all user preferences and changes to settings in the runtime
		LoadPreferences (); // Calling the LoadPreferences() method. This will serch the file containing the user preferences to initialize all the currentlyUserPreferences properties.

		if(playerScript!=null){
			playerScript.energy = currentlyUserPreferences.energyCube; //loads the saving preferences into the energy varibles on the player script
			//playerMovementScript.energyLife = currentlyUserPreferences.energyLife;
			playerScript.arrow = currentlyUserPreferences.arrows;
		}

		// with the currentlyUserPreferences initialized with default or previous saved properties, the Settings Menu UI elements are updated.
		volumeSliders [0].value = currentlyUserPreferences.userMasterVolume; // the userMasterVolume value is assigned to the value of the Master Volume Slider 
		volumeSliders [1].value = currentlyUserPreferences.userMusicVolume; // the userMusicVolume value is assigned to the value of the Music Volume Slider
		volumeSliders [2].value = currentlyUserPreferences.userSfxVolume;  // the userSfxVolume value is assigned to the value of the Sfx Volume Slider

		//saturationSlider.value = currentlyUserPreferences.userSaturation; // the userSaturation value is assigned to the value of the Saturation Slider
		graphicsQuality.value = currentlyUserPreferences.userGraphicsQuality; // the userGraphicsQuality value is assigned to the value of the graphicsQuality DropDown element

		isMuteToggle.isOn = currentlyUserPreferences.userIsMute; // the userIsMute value is assigned to the isOn state of the isMuteToggle toggle element in the scene

		resolutionToggles [currentlyUserPreferences.userResolutionIndex].isOn = true; // True is assigned to the isOn state of the resolutionToggles[] element of index "userResolutionIndex" 
		fullScreenToggle.isOn = currentlyUserPreferences.userIsFullScreen; // the userIsFullScreen value is assigned to the isOn state of the isFullScreen toggle element in the scene

		//ouputSaturation.text = ((int)currentlyUserPreferences.userSaturation).ToString (); // the values of the sliders in the setting menu are casted as integers and also outputed in the output Text objects in the Setting menu
		ouputMasterVolume.text = ((int)currentlyUserPreferences.userMasterVolume).ToString () + "dB";
		ouputMusicVolume.text = ((int)currentlyUserPreferences.userMusicVolume).ToString () + "dB";
		ouputSfxVolume.text = ((int)currentlyUserPreferences.userSfxVolume).ToString () + "dB";
	}

	void Update() // This method is called every frame. We are using this to listen for Pause calls when the active scene has index "1" (play scene). Pause calls do not work during scene (0).
	{
		if(SceneManager.GetActiveScene().buildIndex==1  ) // only if running scene is the one with index (1). Build Index is the index of the scene in the Build Settings
		{	
			
			if (Input.GetKeyDown (KeyCode.Escape)) { // if user presses Esc
				pressedPause = !paused;
				//paused = !paused;
				// Esc both pauses and unpauses changing the boolean "paused".
				if (paused) { // if pause true
					
					myAudioMixer.SetFloat ("masterVolume", -80.0f); // changes the exposed parameter "masterVolume" of the Audio Mixer "myAudioMixer" to -80db.

				} else { // pause false
					cameraScript.follow = false;
					cameraScript.look = false;
					if(pressedPause != paused)
					{
						StartCoroutine (PauseDelay());
					}
					myAudioMixer.SetFloat ("masterVolume", currentlyUserPreferences.userMasterVolume); //  changes the exposed parameter "masterVolume" of the Audio Mixer "myAudioMixer" to the user correspondant preference, previously stored in the .userMasterVolume property.
				}
			}


			myPauseMenu.SetActive (paused); // Activates the pause menu when (paused = true). Deactivates it when (paused = false).

			if (paused) {
				Time.timeScale = 0; // change the scale of time. when is zero it freezes time.

			} else {
				
				Time.timeScale = 1; // change the scale of time to normal.

			}
			
		}
		if(playerScript!=null)
		{
			currentlyUserPreferences.energyCube = playerScript.energy;
		//currentlyUserPreferences.energyLife = playerMovementScript.energyLife;
			currentlyUserPreferences.arrows = playerScript.arrow;
		}

	}



	public IEnumerator PauseDelay()
	{
		
		float time = Time.time;
		Quaternion targetRotation = Quaternion.LookRotation (Vector3.up);
		while (Time.time - time < 3.0f) {
			mainCam.transform.rotation = Quaternion.Lerp(mainCam.transform.rotation,targetRotation,Time.deltaTime*0.5f);
			yield return null;
		}

		paused = !paused;

	}

	public void Unpause(){
		
		pressedPause = !paused;
		paused = !paused;
		myAudioMixer.SetFloat ("masterVolume", currentlyUserPreferences.userMasterVolume); // changes the exposed parameter "masterVolume" of the Audio Mixer "myAudioMixer" to the user correspondant preference, previously stored in the .userMasterVolume property
	}




	public void QuitGame() // This method is called when the button "Quit" in the pause menu or Main menu is pressed, using the "on click event" in the button inspector.
	{
		SavePreferences (); // Calls the SavePreferences method to store all the properties in the currentlyUserPreferences to a Json File
		Application.Quit (); // Quit the Application, the game itself. End of RunTime.
	}

	public void LoadScene() // This method is called when the button "Start Game" in the New Game menu is pressed, using the "on click event" in the button inspector.
	{
		SavePreferences (); // Calls the SavePreferences method to store all the properties in the currentlyUserPreferences to a Json File
		SceneManager.LoadScene (1); // Loads the scene 1 that represents the gameplay scene.
	}
		

	public void NewGame () // This method is called when the button "New Game" in the main menu is pressed, using the "on click event" in the button inspector.
	{
		//myMainMenu.SetActive (false); // Deactivates the main menu
		//myNewGameMenu.SetActive (true); // Activates New game menu
		LoadScene();
	}

	public void SettingsMenu() // This method is called when the button "Settings" in the main menu is pressed, using the "on click event" in the button inspector.
	{
		
		myMainMenu.SetActive (false); // Deactivates the main menu
		mySettingsMenu.SetActive (true); // Activates the settings menu
	}

	public void MainMenu() // This method is called when the button "back" in the settings menu or new game menu is pressed, using the "on click event" in the button inspector.
	{
		mySettingsMenu.SetActive (false); // Deactivates Settings menu
		myNewGameMenu.SetActive (false); // Deactivates New game menu
		myMainMenu.SetActive (true); // activates Main Menu
	}

	public void ChangeName(string NewName) //This method is called when the user enters a new name in the input field inside the new game menu, using the "on end edit" in the inputfield inspector.
	{
		currentlyUserPreferences.userName = NewName; // stores the entered name in the user preferences name, the property .userName of the currentlyUserPreferences object

	}

	public void SetScreenResolution(int i) // This method is called when any of the .isOn states of the resolution Toggles in the settings menu are changed. "i" is the index of the toggle who is calling the method.
	{
		
		if(resolutionToggles[i].isOn) // only if the i toggle is turned to on
		{
			
			currentlyUserPreferences.userResolutionIndex = i; // updates the user preferences. The new active toggle index is assigned to the .userResolutionIndex property.
			float aspectRatio = 16 / 9f; // this is the aspect ration we have chosen to work with.
			Screen.SetResolution (screenWidths [i], (int)(screenWidths [i]/ aspectRatio),false); // sets the screen resolution. screenwidths(i) is the correspondent width of index i, (int)(screenWidths [i]/ aspectRatio) is the calculated height using the aspect ratio, the full screen mode is set to false.

		}
	}
		

	public void SetFullScreen(bool isFullScreen)// Called when Full Screen toggle in the setting menu has its .isOn state changed, using the "on value changed" in the toggle inspector.
	{
		
		currentlyUserPreferences.userIsFullScreen = isFullScreen; // updates the .userIsFullScreen property with new .isOn state, passed as argument.

		for (int i = 0; i < resolutionToggles.Length; i++) 
		{
			resolutionToggles [i].interactable = !isFullScreen; // If Full screen set to on, make all resolution toggles not interactable. The oposite for Full Screen off.
		}

		if (isFullScreen) 
		{
			Resolution[] allResolutions = Screen.resolutions; // create an array of resolutions with all screen resolutions of the application (build settings)
			Resolution maxResolution = allResolutions [allResolutions.Length - 1]; // check which is the biggest resolution
			Screen.SetResolution (maxResolution.width, maxResolution.height, true); // assigns the max width and height resolution to the screen, setting the full screen to true.

		} 
		else 
		{
			SetScreenResolution (currentlyUserPreferences.userResolutionIndex); // if not full screen, calls the SetScreenResolutionMethod to Screen come back to user chosen resolution.
		}

	
	}

	public void SetGraphicsQuality(int i) // Called when DropDown menu in the settings menu has its value changed, receives the index of the option selected
	{

		currentlyUserPreferences.userGraphicsQuality = i; // updates .userGraphicsQuality property

		switch (i) //converts the index in the dropdown menu to Quality Indexes. case o (HIGH) must set the quality level to 5, the maximum in the quality build settings.
		{
			case 0:
				i = 5;
				break;
			case 1:
				i = 3;
				break;
			default:
				i = 0;
				break;
			
		}
	
		QualitySettings.SetQualityLevel (i, false); // Changes the Quality settings, not applying expensive changes for the sytem in the runtime.

	}

	//public void SetSaturation(float value) // called when the value of the saturation slider in the settings menu is changed, using the "On Value changed" in the slider inspector
	//{
	//	//mainCam.GetComponent<ColorCorrectionCurves> ().saturation = value; // sets the value in the slider to the .saturation property of the class ColorCorrectionCurve, assigned to the MainCamera
	//	currentlyUserPreferences.userSaturation = value; //Updates user LoadPreferences object
	//	ouputSaturation.text = ((int)value).ToString (); // output value in Text Object
	//}

	public void SetMasterVolume(float value) // called when the value of the Master volume slider in the settings menu is changed, using the "On Value changed" in the slider inspector
	{
		myAudioMixer.SetFloat ("masterVolume", value); // Changes the exposed parameter "masterVolume" of the Audio Mixer to the value of the slider passed as argument
		ouputMasterVolume.text = ((int)value).ToString () + "dB"; // output value in Text Object
		if(isUserSliding0) // updates user preferences only if the user is sliding, not the MuteVolume() method.
			currentlyUserPreferences.userMasterVolume = value;

		isUserSliding0 = true; // resets isUserSliding after updating value

	}
		
	public void SetMusicVolume(float value) // called when the value of the Music volume slider in the settings menu is changed, using the "On Value changed" in the slider inspector
	{
		ouputMusicVolume.text = ((int)value).ToString () + "dB"; // output value in Text Object
		myAudioMixer.SetFloat ("musicVolume", value); // Changes the exposed parameter "musicVolume" of the Audio Mixer to the value of the slider passed as argument

		if(isUserSliding1) // updates user preferences only if the user is sliding, not the MuteVolume() method.
		currentlyUserPreferences.userMusicVolume = value;
		
		isUserSliding1 = true; // resets isUserSliding after updating value

	}

	public void SetSfxVolume(float value) // called when the value of the Sfx volume slider in the settings menu is changed, using the "On Value changed" in the slider inspector
	{
		ouputSfxVolume.text = ((int)value).ToString () + "dB"; // output value in Text Object
		myAudioMixer.SetFloat ("sfxVolume", value); // Changes the exposed parameter "sfxVolume" of the Audio Mixer to the value of the slider passed as argument

		if(isUserSliding2) // updates user preferences only if the user is sliding, not the MuteVolume() method.
			currentlyUserPreferences.userSfxVolume = value;

		isUserSliding2 = true; // resets isUserSliding after updating value

	}



	public void MuteVolume(bool isMute) // this method is called when the toggle Mute odf the Settings menu has its isOn value changed, using the "on value changed" in the toggle inspector
	{
		isUserSliding0 = false; // the muteVolume is gonna change the sliders values, setting this to false prevents it from saving the new volunmes to user preferences
		isUserSliding1 = false;
		isUserSliding2 = false;
		currentlyUserPreferences.userIsMute = isMute; // updates the user preferences

		if (isMute) {
			for (int i = 0; i < volumeSliders.Length; i++) {
				volumeSliders [i].interactable = false; // if mute is clicked, make all volume sliders not interactable and turn all volume to -80db
				volumeSliders [i].value = -80.0f;
			}


		} else
		{
			for (int i = 0; i < volumeSliders.Length; i++) // if toggle unclicked, make all volume sliders interactable, return all volume values to the saved user preferences values
			{
				volumeSliders [i].interactable = true;
			}
			volumeSliders [0].value = currentlyUserPreferences.userMasterVolume;
			volumeSliders [1].value = currentlyUserPreferences.userMusicVolume;
			volumeSliders [2].value = currentlyUserPreferences.userSfxVolume;

		}
	}

	public void LoadPreferences() //When this MethodAccessException is called, it searches for a json file with user preferences and loads it into an object, or assigns default values to the object properties if file not found
	{
		if (File.Exists (filePath + "/" + FILE_NAME)) { //if File filePath/UserPreferences.json exists
			string LoadedJson = File.ReadAllText (filePath + "/" + FILE_NAME); // Reads the file and store it in a string
			currentlyUserPreferences = JsonUtility.FromJson<UserPreferences> (LoadedJson);//The method .FromJson converts the json format into a object of class UserPreference and stores it on the currentlyUserPreferences object
			Debug.Log ("File Loaded");

		} else // If File not found, assigns default values to the currentlyUserPreferences properties.
		{
			currentlyUserPreferences.userMasterVolume = -10.0f;
			currentlyUserPreferences.userMusicVolume = -10.0f;
			currentlyUserPreferences.userSfxVolume = -20.0f;
			currentlyUserPreferences.userIsMute = false;
			currentlyUserPreferences.userGraphicsQuality = 0;
			currentlyUserPreferences.userResolutionIndex = 0;
			currentlyUserPreferences.userIsFullScreen = false;
			currentlyUserPreferences.userName = "Your Name";
			currentlyUserPreferences.energyCube = 0;
			currentlyUserPreferences.arrows = 2;

		}

		
	}

	public void SavePreferences() // When called, this method saves the user preferences to a json file
	{
		string SaveJson = JsonUtility.ToJson (currentlyUserPreferences); // the currentlyUserPreferences object, containg all the user preferences properties is converted to a json format with the method .ToJson and saved on the created string "SaveJson"
		File.WriteAllText (filePath + "/" + FILE_NAME, SaveJson); // The method WriteAllText from the File class (System.IO Library) Write the SaveJson string in the path filePath/UserPreferences.json
		Debug.Log ("File Saved");
		Debug.Log (SaveJson);

	}



}
