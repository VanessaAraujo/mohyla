﻿using UnityEngine;
using System.Collections;

/// <summary>
/// //THIS SCRIPT HANDLED THE IK FOR THE FOOT PLACEMENT AND THE MATCH TARGET ON CLIMBING. IT ALSO CONTROSL THE ROTATION OF THE SPINE WHILE ON AIMING STATE
/// </summary>
public class IKController : MonoBehaviour {

	public bool footIK = true; // if foot is allowed, this is true
	public bool aim; // flag the aiming
	public bool shoot; // flag a shot
	public Transform cameraTransform; // transform of the camera
	public bool ledge; // flag for a high wall for climbing on front of player
	public Transform playerTrackAim;//transform of THE PLAYER TRACKER
	public Transform bowHand; // the hand that holds the bow

	private bool rightFootIK; // flag to allow right foot ik
	private bool leftFootIK;//flag to allow left foot ik
	private float rightFootiKWeight = 1.0f;//weight for the right foot ik
	private float leftFootiKWeight = 1.0f;//weight for the left foot ik
	private float footOffSet = 0.05f;// distance from the target point
	private Animator anim;//the player animator
	private LayerMask footLayerMask;//the mask to ignore player layer
	private float tiltAngle;//vertical rotation of aiming 
	private float lookAngle;//horizontal rotation of aiming 
	private float bodyTiltAngle; //vertical rotation of  spine
	private float bodyLookAngle; //horizontal rotation of  spine
	private Vector3 spineLocalAngle;//the rotation of the spine
	private Vector3 spineOriginalLocalAngle;//the original state of spine before aiming
	private Transform rightFoot;//rightfoot joint
	private Transform leftFoot;
	private Vector3 rightFootPosTarget;
	private Vector3 leftFootPosTarget;
	private Quaternion rightFootRotTarget;
	private Quaternion leftFootRotTarget;
	private Transform spine;
	private Transform root; // root of the body
	private Rigidbody playerRigidbody;
	private Vector3 playerTrackerOriginalLocalPosition; // original position of the tracker
	private Quaternion playerTrackerOriginalLocalRotation;
	private PlayerMovement playerMovementScript; // script of the player movement
	private RaycastHit rightHandLedgeHit; // hit of the raycast on the right
	private RaycastHit leftHandLedgeHit;
	private RaycastHit leftHandLedgeTarget; //// the target for the matchtarget
	private bool matchAnimation;//if matchtarget is allowe
	private bool unMatchAnimation;
	private bool startAim; // true on the first frame of aiming state



	void Start() {

		footLayerMask = 1 << 8;	//the layer 8
		footLayerMask = ~footLayerMask;//all but the 8 layer

		anim = GetComponent<Animator> (); // initialization
		playerRigidbody = GetComponent<Rigidbody> ();
		rightFoot = anim.GetBoneTransform (HumanBodyBones.RightFoot);
		leftFoot = anim.GetBoneTransform (HumanBodyBones.LeftFoot);
		playerMovementScript = GetComponent<PlayerMovement> ();
		spine  = anim.GetBoneTransform (HumanBodyBones.Spine);

		spineLocalAngle = spine.localEulerAngles; // stores the local rotation of the spine
		spineOriginalLocalAngle = spine.localEulerAngles; // stores the local rotation of the spine
		playerTrackerOriginalLocalPosition = playerTrackAim.localPosition; // // stores the original local position of tracker
		playerTrackerOriginalLocalRotation = playerTrackAim.localRotation; // stores the original local rotation of tracker


	}
		
	void FixedUpdate()
	{

		//////////////////////////////////////////////HANDLES THE MATCH TARGET FOR THE CLIMBING ANIMATION
		RaycastHit rightFootHit;
		RaycastHit leftFootHit;
		rightFootPosTarget = rightFoot.TransformPoint (Vector3.zero); // the position of the foot in word space
		leftFootPosTarget = leftFoot.TransformPoint (Vector3.zero);


		if (Physics.Raycast (rightFoot.position, -Vector3.up, out rightFootHit, 0.3f, footLayerMask)) {
			
			rightFootIK = true; // if the ray from the player position towards the ground hits something that is not the player, ik is allowed
			rightFootPosTarget = rightFootHit.point; // //target of ik is the point of ray hit
			rightFootRotTarget = Quaternion.FromToRotation (transform.up, rightFootHit.normal) * transform.localRotation;//finds how much rotation between the hit normal and the up transform of the player and translates this to the local rotation
		} else {
			rightFootIK = false;
		}

		if(Physics.Raycast(leftFoot.position, -Vector3.up, out leftFootHit, 0.3f, footLayerMask))
		{
			
			leftFootIK = true;
			leftFootPosTarget = leftFootHit.point;
			leftFootRotTarget = Quaternion.FromToRotation (transform.up, leftFootHit.normal) * transform.localRotation;
		} else {
			leftFootIK = false;
		}


		///////////////////////////////////////////////////HANDLES THE LEDGE SENSOR
		ledge = false;
		if (Physics.Raycast (transform.position + transform.forward * 0.4f + transform.up * 3.0f, -transform.up + transform.right * 0.25f, out rightHandLedgeHit, 2.5f)) {
			if (rightHandLedgeHit.transform.tag == "Platform") {
				if (Physics.Raycast (transform.position + transform.forward * 0.4f + transform.up * 3.05f, -transform.up - transform.right * 0.25f, out leftHandLedgeHit, 2.5f)) {
					if (leftHandLedgeHit.transform.tag == "Platform") {
						ledge = true; //if both raycasts from the front-up-right to the bottom and front-left -side to the bottom hit something with tag "platform" ledge is true
					}
				}
			}
					
		}
	
		playerMovementScript.ledge = ledge; // updtase the ledge bool in the player script, so it player the climb animation, instead of the jump animation					
	}

	void OnAnimatorIK()
	{
		//////////////////////////////////////////////HANDLES THE MATCH FOOT IK
		if (matchAnimation) { // if the body is doing matchtarget, do not do foot ik, instead,match the leftfoot with the point whenre the ledge raycast hit the wall starting on the 0.1f instante of animation and matching on the 0.7, this is equivalent to 10% and 70%
			footIK = false;
			anim.MatchTarget (leftHandLedgeTarget.point, Quaternion.LookRotation(transform.forward), AvatarTarget.LeftFoot, new MatchTargetWeightMask (Vector3.one, 1f), 0.1f, 0.7f);
		} else if(unMatchAnimation) { // true while the matchtarget is set to true
			
			anim.InterruptMatchTarget(true);//stop the matchtarget 
			footIK = true;//allows the ik again
			unMatchAnimation = false;//match target already unmatched
		}


		/////////////////////////////////////  HAND IK
		/// 
		if (aim && shoot==false) { //if it is aiming And Not shooting or loading

			anim.SetIKPositionWeight (AvatarIKGoal.RightHand, 1.0f);
			anim.SetIKPosition (AvatarIKGoal.RightHand, bowHand.position ); // sets ik weight to 1  and its position to the position it should have while aiming parented to the bow

		} 

			anim.SetIKPositionWeight (AvatarIKGoal.RightHand, 0.0f);


		rightFootiKWeight = anim.GetFloat ("rightFoot");
		leftFootiKWeight = anim.GetFloat ("leftFoot");

		///////////////////////////// FOOT IK
		if(footIK)
		{
			if (rightFootIK) { // sets weight and position and rotation of the foot joint. weight comes  from movement animation curves, position comes from raycasting, done before
				anim.SetIKPositionWeight (AvatarIKGoal.RightFoot, rightFootiKWeight);
				anim.SetIKPosition (AvatarIKGoal.RightFoot, rightFootPosTarget + new Vector3 (0, footOffSet, 0));

				anim.SetIKRotationWeight (AvatarIKGoal.RightFoot, rightFootiKWeight);
				anim.SetIKRotation (AvatarIKGoal.RightFoot, rightFootRotTarget);
			}

			if (leftFootIK) {
				anim.SetIKPositionWeight (AvatarIKGoal.LeftFoot, leftFootiKWeight);
				anim.SetIKPosition (AvatarIKGoal.LeftFoot, leftFootPosTarget + new Vector3 (0, footOffSet, 0));

				anim.SetIKRotationWeight (AvatarIKGoal.LeftFoot, leftFootiKWeight);
				anim.SetIKRotation (AvatarIKGoal.LeftFoot, leftFootRotTarget);
			
			}
		}

		//////////////////////////////////////////////HANDLES THE ROTATION OF SPINE FOR CAMERA TO FOLLOW
		if (aim) {
			if (!startAim) {
				spineLocalAngle.z += 15.0f; // when first press left mouse, adjust the spine joint rotation to match the range of rotation we want on the screen
				spineLocalAngle.y += 15.0f;
			}
			startAim = true;

			float x = Input.GetAxis ("Mouse X"); // the delta of the position of the mouse cursor
			float y = Input.GetAxis ("Mouse Y"); 

			tiltAngle -= y; // decrements  rotation of spine proportionaly to the delta
			lookAngle += x;

			bodyTiltAngle = spine.localEulerAngles.z + tiltAngle; // using another variable to be clamped right ahead. it contain the spine rotation plus the sum of the increments of deltas
			bodyLookAngle = spine.localEulerAngles.y + lookAngle;
		
			if (spineLocalAngle.z > 360.0f) // adjust value for unity to understand. work with small rotations. ex angle = 355, uses -5 instead
				spineLocalAngle.z -= 360.0f;

			if (spineLocalAngle.y > 360f)
				spineLocalAngle.y -= 360.0f;
			
			bodyTiltAngle = Mathf.Clamp(bodyTiltAngle, spineLocalAngle.z -50.0f , spineLocalAngle.z + 50.0f); // clamp the tilt and look no to distort too much the body mesh
			bodyLookAngle = Mathf.Clamp(bodyLookAngle, spineLocalAngle.y -65.0f , spineLocalAngle.y + 50.0f);

			Quaternion targetRotation = Quaternion.Euler (spine.localEulerAngles.x, bodyLookAngle, bodyTiltAngle ); // transform to quaternion
			anim.SetBoneLocalRotation (HumanBodyBones.Spine, targetRotation); // apply the rotation to the spine who parents the upper body parts

			playerTrackAim.parent = spine; // parents the player track to the spine... so the camera can follow it


		} else {
			startAim = false; //when not aiming, return to original rotation
			spineLocalAngle = spineOriginalLocalAngle;

			anim.SetBoneLocalRotation (HumanBodyBones.Spine, Quaternion.Euler(spineLocalAngle));
			playerTrackAim.parent = transform;//return to player parent
			playerTrackAim.localPosition = playerTrackerOriginalLocalPosition; // return player track to original local position and rotation, altered after the parent changing
			playerTrackAim.localRotation = playerTrackerOriginalLocalRotation;
			tiltAngle = 0;//deltas re initialized
			lookAngle = 0;

		}
	}

	/// <summary>
	/// ///////////////////////////////////////////////////////////////////
	/// </summary>

		//enable and disable rigidbody in order to do the matchtarget right
	public void EnableRigidbody() { 
		playerRigidbody.isKinematic = false;
		playerRigidbody.detectCollisions = true;
	}
	public void DisableRigidbody() {
		playerRigidbody.isKinematic = true;
		playerRigidbody.detectCollisions = false;
	}

	public void MatchAnimation(){
		matchAnimation = true;

	}

	public void DisableMatchAnimation(){
		
		matchAnimation = false;
		unMatchAnimation = true;

	}
	public void SaveLedgeTarget(){ // called by the player script, to save the hand ledge target to the raycast hit 
		
		leftHandLedgeTarget = leftHandLedgeHit;
	}

}
