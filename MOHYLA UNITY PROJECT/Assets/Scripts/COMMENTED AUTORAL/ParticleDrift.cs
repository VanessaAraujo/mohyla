﻿using UnityEngine;
using System.Collections;

/// <summary>
/// ADDS SPEED TO THE PARTICLES AFTER 4 SECONDS AND DESTROY THEM AFTER 7 SECONDS OF START LIFE
/// </summary>
public class ParticleDrift : MonoBehaviour {

	ParticleSystem particleSystemToDrift; // The particle system to drift
	ParticleSystem.Particle[] myParticles; // initialize a list of particles from the particle sytema

	float startTime;
	float driftSpeed = 0; // the current forced speed
	float maxDriftSpeed = 10.0f;//he final speed of each particle
	float time = 0; 


	void Start () {

		particleSystemToDrift = GetComponent<ParticleSystem>();
		myParticles = new ParticleSystem.Particle[particleSystemToDrift.maxParticles]; // 

		startTime = Time.time; //the time, now

		Destroy (gameObject, 7.0f); // destroys the particles after 7.0f
	
	}
	

	void LateUpdate () {

		if((Time.time-startTime)>4.0f){
			int numParticles = particleSystemToDrift.GetParticles(myParticles); // after 4 .0f takes all alive particles and drift them
			time += Time.deltaTime;

			driftSpeed = Mathf.Lerp (driftSpeed, maxDriftSpeed, time * 0.5f); // lerps the speed to drift
				
			
				
				for (int i = 0; i < numParticles; i++)
				{
				myParticles[i].velocity += Vector3.up * driftSpeed; // apply the speed on each one of the particles
				}

				
				particleSystemToDrift.SetParticles(myParticles, numParticles); // sets the updated particles to the particles system
				
		}
	}
	

}
