﻿using UnityEngine;
using System.Collections;
using System;

public class AnimalAI : MonoBehaviour {

	/// <summary>
	/// SCRIPT CONTROLS THE DEER AI
	/// </summary>
	public enum State{ //States for the Deer AI control
		PATROL,
		CHASE,
		ATTACK,
	}

	public PlayerMovement playerMovementScript; // reference to Player Script attached to player

	public float patrolSpeed = 0.5f; //agent speed on Patrol state
	public float chaseSpeed; // agent speed on chase state
	public float reachDistante = 0.2f; // minimum distance to be reached before changing waypoint target
	public State state; // the variable that stores the current state of the deer
	public bool alive = true; // a flag for the deer death
	public Transform[] waypoints; // array of waypoints to be used by the Nav agent during PATROL state
	public Transform target; // reference to a gameobject parented to the player, to be tracked by the deer on CHASE and ATTACK states

	private NavMeshAgent agent; // The NavMeshAgent constrols the movement of the deer through the navmesh
	private int waypointIndex = 0; // index of first waypoint
	private Animator anim; // the deer animator
	private Quaternion targetRotation; // the rotation of the vector with origin on the deer and end on the  target
	private Rigidbody animalRigidbody; // animal rigidbody
	private Vector3 lastRotation; // the rotation on the last keyframe
	private Vector3 deltaRotation; // the diference in rotation between the current rotation and the one on the last frame
	private float rotationSpeed = 0.5f; // the rotation speed when the 
	private float lastAttackTime; // its stores the time of the last attack to keep attacking rate above a certain rate
	private float attackInterval = 2.0f; // the deer can attack on intervals of two sends


	void Start () {

		target = GameObject.FindWithTag("Player").transform; //the player transform is set to the target

		playerMovementScript = target.GetComponent<PlayerMovement> (); // accessing the script attached to the player
		agent = GetComponent<NavMeshAgent> (); // the agent attached to deer
		anim = GetComponent<Animator> (); // deer animator
		animalRigidbody = GetComponent<Rigidbody> ();//deer rigidbody
	
		lastRotation = transform.rotation.eulerAngles;// the current rotation of the deer in euler angles


		agent.updatePosition = true; // the agent will update deer's position, but the rotation will be controlled here, in order to try to match the most we can to the turning animations.
		agent.updateRotation = false;

		state = State.PATROL; // initializes the deer state to patrolling

		agent.speed = patrolSpeed; // this is the speed the agent will feed to the deer object
		waypointIndex = UnityEngine.Random.Range (0, waypoints.Length); // randomly selects between zero and 3, the result will be smaller or equal to two
		agent.SetDestination (waypoints [waypointIndex].position); // initializes the agent destination to the randomly selected waypoint
		StartCoroutine ("FSM"); // calling the coroutin e that will call the respective functions according to the current state
		lastAttackTime = Time.time; // initializes the last attack

	}

	void Update()
	{
		
		deltaRotation = transform.rotation.eulerAngles - lastRotation; // lastrotation receives the current rotation in euler angles
		lastRotation = transform.rotation.eulerAngles;
	
	}
	

	void LateUpdate () {
	//
		Vector3 agentProjection = Vector3.ProjectOnPlane (agent.velocity, Vector3.up).normalized; // a projection of the agent velocity over the horizontal plan
		float hMove = deltaRotation.y; // we are interested only on the rotation aroun the Y

		Quaternion targetRotation = Quaternion.LookRotation(agentProjection); // a rotation that would make the forwar of the deer equal to the forward of the agent speed, since the agent is following the target without worrying about rotation of the rigidbody
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime*rotationSpeed); // interpolates the deer rotation to the target rotation  using the rotation Speed
		anim.SetFloat("hMove",hMove);//the amout of hmove tells how much the deer should turn and which way, the value is passed to the animator that will call the respective animation.

		if(!alive) // calls the Dye method if alive is set to false
			Die();
	}

	IEnumerator FSM()
	{
		while (alive) //while the deer is alive it switches between the states on each frame, calling the handling function respective  to the state
		{
			switch (state) 
			{
			case State.PATROL:
				Patrol();
				break;
			case State.CHASE:
				Chase();
				break;
			case State.ATTACK:
				Attack ();
				break;
			}
			yield return null;
		}

	}

	void Patrol()
	{
		anim.SetBool ("chase", false); // sets the animation to patrolling
		if (agent.remainingDistance > reachDistante) { // if far to the target
			agent.SetDestination (waypoints [waypointIndex].position); // //update the same target


		} else  {

			waypointIndex = UnityEngine.Random.Range (0, waypoints.Length); // if its closed, ramdonmly picks one of the waypoints to follow and updates the agent
			agent.SetDestination (waypoints [waypointIndex].position);
		}


	}

	void Chase()
	{	

		agent.SetDestination (target.position); // chases the player position
		anim.SetBool ("chase", true); //plays chase animation and updates the speeds of the agente and the rotation one used in the script
		rotationSpeed = 2.0f;
		agent.speed = 3.0f;
		if (agent.remainingDistance <= 1.0f && playerMovementScript.energyLife>0) { // if close to player and player has energy, attack
			state = State.ATTACK;	 
		}
		if(playerMovementScript.energyLife==0) //if player has no left energy, go bak to patrolling
			state = State.PATROL;
	}

	void Attack()
	{	
		

		anim.SetBool ("chase", false);
		if (Time.time - lastAttackTime >= attackInterval && playerMovementScript.energyLife>0) { //if its more RuntimeTypeHandle 2.0f since the lastRotation Attack AndroidJNI the playerMovementScript Hash128 the energy, attack
			lastAttackTime = Time.time;
			anim.SetTrigger ("attack2");
		}
		agent.speed = 1.0f;
		agent.SetDestination (target.position);
		if (agent.remainingDistance > 1.5f && playerMovementScript.energyLife>0) { //if its far and the player has energy, chases it
			state = State.CHASE;

		}

		 if(playerMovementScript.energyLife==0) // if play is out of energy, patrol
			state = State.PATROL;
		

	}




	void OnTriggerEnter(Collider col)

	{
		if (col.tag == "Player" && alive)  // if colides with the player and player has energy, chase
		{
			
			if(playerMovementScript.energyLife>0)
			state = State.CHASE;


		}
	}

	void Die()
	{
		anim.SetBool ("die", true); //when alive is false, updates animation and stops the agent by setting its own position ast the target and stops rotating
		agent.SetDestination (transform.position);
		rotationSpeed= 0;
		Invoke ("DisableRigidbody",1.0f);
		Destroy (gameObject,6.0f); // destroy deer object in 6 seconds to match the animtion and particles



	}





	void DisableRigidbody()
	{
		animalRigidbody.isKinematic = true; // this method is called using a delay with invoke, it settle the rigidbody to be kinematic
	}

	public void Hit(){ // this function is called by the event system of the animation on mecanim write when the attack animation beggins
		if(agent.remainingDistance < 1.5f) // if the player is closed, damage it using a public function from the player scrip
			playerMovementScript.HitDamage ();
	}
}
