﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


/// <summary>
/// MANAGES THE PUZZLE BY LISTENING TO THE LEVERS STATES AND MOVING THE BIG BRIDGE IF THEY ARE ALL DOWNS, AND SPAWNING A NPC IF TWO LEVERS ARE DOWN
/// </summary>

public class PuzzleManager : MonoBehaviour {

	public Transform rockBridge; // the position of the big bridge in starting state, and the positions for going up or down
	public Transform rockBridgeUp;
	public Transform rockBridgeDown;

	public GameObject monumentOne; // references to the lever monuments
	public GameObject monumentTwo;
	public GameObject monumentThree;

	public GameObject deer; // the deer
	public Transform deerStartPosition; // the deer position

	public bool monumentOneActivated; // it will receive the lever states for each monument
	public bool monumentTwoActivated;
	public bool monumentThreeActivated;

	public bool deerCreated = false;
	public bool end;
	public AudioSource bridgeAudio;




	
	// Update is called once per frame
	void Update () {



		monumentOneActivated = !(monumentOne.GetComponent<LeverControl>().up); // a lever that is up its not activated
		monumentTwoActivated = !(monumentTwo.GetComponent<LeverControl>().up);
		monumentThreeActivated = !(monumentThree.GetComponent<LeverControl>().up);




		if (monumentOneActivated && monumentTwoActivated && monumentThreeActivated) {
			
			rockBridge.position = Vector3.MoveTowards (rockBridge.position, rockBridgeDown.position, Time.deltaTime * 2.0f); // if all three levers pushed down, the bridge changes position
			if (!end) {
				Invoke ("EndOfGame", 10.0f);
				bridgeAudio.Play ();
			}
			end = true;
		} 

		if (monumentTwoActivated && monumentThreeActivated && !deerCreated ) {
			Instantiate (deer, deerStartPosition.position, Quaternion.LookRotation (Vector3.forward)); // if the two levers on the top of the cliff  are activated, the a deer is instantiated in the scene.
			deerCreated = true; // this keeps the script from instantiating again.
		}

	
	}



	void EndOfGame(){
		SceneManager.LoadScene (0);
		
	}

}
