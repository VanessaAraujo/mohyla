﻿using UnityEngine;
using System.Collections;

/// <summary>
/// ROTATES THE OBJECT AROUND THE THREE AXIS ON CHOSEN SPEEDS
/// </summary>
public class AutoRotate : MonoBehaviour {

	public float rotationSpeedX = 45.0f;
	public float rotationSpeedY = 45.0f;
	public float rotationSpeedZ = 45.0f; // rotating speeds on each axis



	void Update () { // updates rotation, addin a constant increment to the rotation
	
		transform.Rotate(rotationSpeedX * Time.deltaTime, rotationSpeedY * Time.deltaTime, rotationSpeedZ * Time.deltaTime, Space.World);
	}
}
