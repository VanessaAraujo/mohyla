﻿using UnityEngine;
using System.Collections;


/// <summary>
/// CONTROLS THE LEVER STATES, MOVEMENT AND MATERIAL
/// </summary>
public class LeverControl : MonoBehaviour {


	public enum State{ // The possible states of the Lever position
		UP,
		DOWN
	}
	public bool alive = true; // this will keep the FSM coroutine working and can be stopped whenever needed
	public bool up = true; // lever is in up position
	public bool goUp = true; // player is seting the lever to up

	public Transform positionUp;//the tranform of the up and down position
	public Transform positionDown;

	public GameObject monumentTexture; //the object to have the material changed
	public Material monumentOnMaterial; // material to be used when lever is up
	public Material monumentOffMaterial;



	public State state = State.UP;
	// Use this for initialization
	void Start () {
		StartCoroutine ("FSM");	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator FSM()
	{
		while (alive)// loops through the states while alive and call the respective method
		{
			switch (state) 
			{
			case State.UP:
				LeverUp();
				break;
			case State.DOWN:
				LeverDown();
				break;
			}
			yield return null;
		}

	}

	void LeverUp(){ //LeverUp handler
		
		up = true; // currently state is up
		if (Vector3.Distance (transform.position, positionUp.position) > 0.05f) { // while not yet in the up position, move to that point with a constant speed
			transform.position = Vector3.MoveTowards (transform.position, positionUp.position, Time.deltaTime * 0.5f);
		} else {
			monumentTexture.GetComponent<Renderer>().material=monumentOffMaterial; // if its up, material is off
		}
		if (!goUp) { // go up is update on the player movement after user input. 
			
			state = State.DOWN;
		}

	}

	void LeverDown(){ //The same behavior handles the down position
		
		up = false;
		if (Vector3.Distance (transform.position, positionDown.position) > 0.05f) {
			transform.position = Vector3.MoveTowards (transform.position, positionDown.position, Time.deltaTime * 0.7f);
		} else {
			monumentTexture.GetComponent<Renderer>().material=monumentOnMaterial;
		}
		if (goUp) {
			

			state = State.UP;
		}


	}

}
