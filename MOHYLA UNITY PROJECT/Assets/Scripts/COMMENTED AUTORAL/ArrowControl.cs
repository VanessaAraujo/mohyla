﻿using UnityEngine;
using System.Collections;


/// <summary>
/// ARROW CONTROL FOR DIRECTION AND PARENTING AFTER COLLISION, AS WELL AS HIT HANDLING
/// </summary>
public class ArrowControl : MonoBehaviour {

	public ParticleSystem deerDeathParticle; //particle sytem for the deer death
	public ParticleSystem deerHitParticle; // and for the deer hit
	public float flyTime = 10.0f; // max fly time
	public bool flying = false;

	private GameObject anchor; // this will help us parenting the arrow later on
	private BoxCollider arrowCollider; // the collider attached to the arrow
	private Rigidbody arrowRigidbody; // the arrow's rigidbody


	void Start () {
		
		arrowCollider = GetComponent<BoxCollider> (); // initialization
		arrowRigidbody = GetComponent<Rigidbody> ();

	
	}
	

	void Update () {

		if (flying) { 

			rotate ();
		
			
		} else if (anchor != null) {
			transform.position = anchor.transform.position; //gets the position and rotation of the anchor to itself
			transform.rotation = anchor.transform.rotation;
		}
	
	}


	void OnCollisionEnter(Collision col)
	{
		
		if (flying && col.transform.name!="Player" ) 
		{
			arrowRigidbody.velocity = Vector3.zero;  // if colide with something that is not the player during flight, stop flying, changes its position to the colision first contact point and sets the collider to trigger
			flying = false;
			transform.position = col.contacts [0].point;
			arrowCollider.isTrigger = true;

			GameObject newAnchor = new GameObject ("Arrow Anchor"); //creates a new anchor  to place the anchor transform
			newAnchor.transform.position = transform.position;
			newAnchor.transform.rotation = transform.rotation;
			newAnchor.transform.parent = col.transform; // parents the new anchor to the collider transform and then the old anchor receives all that, bringing the arrow together
			anchor = newAnchor;

			Destroy (arrowRigidbody); // 

			if (col.transform.name == "Deer") { // if hits the deer, instantiate particles and sets the deer alive bool to false;
				Instantiate(deerHitParticle, col.transform.position, Quaternion.LookRotation(Vector3.forward));
				col.gameObject.GetComponent<AnimalAI> ().alive = false;
				StartCoroutine (DelayParticle(col.gameObject));


			}
		}
		
	}

	IEnumerator DelayParticle(GameObject col){ // receives a colider in which to position the particles
		
		yield return new WaitForSeconds (1.0f); // 1.0f delay
		Instantiate(deerDeathParticle, col.transform.position, Quaternion.LookRotation(Vector3.forward)); // instantiate particles matching its forward direction to the wordpace forward
	}

	void rotate()
	{
		transform.LookAt (transform.position + arrowRigidbody.velocity); // rotates the arrow  to match velocity direction
	}
}
