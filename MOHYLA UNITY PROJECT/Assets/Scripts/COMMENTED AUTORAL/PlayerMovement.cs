﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// MAIN SCRIPT FOR THE PLAYER MOVEMENT, INPUT CONTROL AND PICK UPS
/// </summary>
public class PlayerMovement : MonoBehaviour {



	public CameraFollow cameraScript; // camera script
	public IKController IKScript; // IK script
	public Camera mainCamera; // main camera
	public GameObject energyParticles; // particle system prefab of picking energy cube
	public bool ledge; // true if there is a climbing wall forward to the player
	public MenuManager menuManagerScript; // the sript that controls the main menu
	public int energy; // the number of cubes of  energy in the inventory
	public int arrow; // the number of arrows in the inventory
	public Text energyLevelText; // text box in the scene that outputs the number of energy cubes
	public Text arrowLevelText; // text box that outputs the number of arrows
	public Image energyLifeImage;// the image to be filled proportionally to the amount of life energy
	public float energyLife=1.0f; // correspondes to 100% life energy
	public bool energyCharge; // tru if player coliding with the blue fire particles in the scene, tyhat recharges its life energy
	public AudioClip hitSound; // audio clip for the hit by deer
	public AudioClip energySound;// audio clip for the picking energy

	public ParticleSystem damageParticle; // the particle system prefab to be spawn when damaged
	private Transform righthand;// reference to the right hand joint
	private float jumpTime;//time of last jump
	private float hMovement;//vertical arrow value
	private float vMovement;//horizontal arrow value
	public bool lever;//true if raycasts a lever object in front of player
	private bool jump = false;// sets the player to jump state
	private bool run = false;//sets the player to run
	private bool aim = false;//flag for aiming
	private bool paused;
	private BowControl bowControlScript;// the script that controls the arrow spawning
	private Animator anim;// the animator attached to the player
	private Rigidbody playerRigidbody;//player rigidbody
	private AudioSource audioSource;//audiosource attached to the player
	private string arrowRoman; // string that will keep the number of arrows in roman numbers
	private string energyRoman; // string that will store the energy cubes quantity in roman numbers

	public AudioClip lockedMonumentAudio;
	public AudioClip loadingEnergyAudio;
	public AudioClip leverPushAudio;

	void Start () {
		
		playerRigidbody = GetComponent<Rigidbody> (); //initialization
		audioSource = GetComponent<AudioSource> ();
		anim = GetComponent<Animator> ();
		bowControlScript = GetComponentInChildren<BowControl> ();
		righthand = anim.GetBoneTransform (HumanBodyBones.RightHand);

		arrow = bowControlScript.numberOfArrows; // the bow script controls the number of available arrows

		if (arrow > 0) {
			arrowRoman = IntToRoman (arrow); // converts to roman numbers if greater than zero
		} else
			arrowRoman = "O"; //otherwise, use the letter "o" intead of the number zero, aestetic reason
		
		arrowLevelText.text =  arrowRoman; // outputs the number of arrows
		energyLife = 1.0f; // initializes the amount of life energy
		energyLifeImage.fillAmount = energyLife; // fills the output image proportionally, between 0 and 1

		energy = 0;
	}

	void Update() {

		///////////////////////// HUD OUTPUTS AND PICK UPS NUMBER
		/// 
		if (energy > 0) {

			energyRoman = IntToRoman (energy);//converts number to roman before outputting
			} else {

				energyRoman = "O";
		}
		energyLevelText.text = energyRoman;
		if (energyCharge)// if colide with the charger
			energyLifeImage.fillAmount += 0.1f;//increase life energy 0.1 on every frame
		else {
			if(run)
			energyLifeImage.fillAmount -= 0.001f;//otherwise decrease 0.0007 per frame if player running
		}

		energyLife = energyLifeImage.fillAmount;//fills the output image
	

		if (arrow > 0) {
			arrowRoman = IntToRoman (arrow);//if there is any arrows, convert the amount to roman, or to the letter "o" otherwise
		} else {
			arrow = 0;
			arrowRoman = "O";
		}

		arrowLevelText.text = arrowRoman; //outputs to the text box

		////////////////////////////////////////// the INPUTs that are locked if on pause

		paused = menuManagerScript.pressedPause; // true if player pressed esc

		if (!paused && SceneManager.GetActiveScene ().buildIndex != 0) { // only take movement command inputs from mouse and keyboard if its not paused and if the scene is not the menu secen
			vMovement = Input.GetAxis ("Vertical");// stores the vertical arrows inputs
			hMovement = Input.GetAxis ("Horizontal");// stores the horizontal arrows inputs

			if (Input.GetMouseButtonDown (0)) {
				
				StartCoroutine (cameraScript.SwitchToAim ()); // if left mouse button clicked, calls method of script of the camera to start aiming
			}
				
		    if (Input.GetMouseButtonUp (0)) {
				StartCoroutine (cameraScript.SwitchFromAim ()); // if left mouse button unclicked, calls method of script of the camera to stop aiming
	    	}

			run = Input.GetButton ("Fire3"); // runs if shift pressed

			if (energyLife == 0) { // running locked if no life energy left
				run = false;
			}

			aim = Input.GetMouseButton (0); // true while left mouse button pressed

			cameraScript.aim = aim; // update state on camera script
			IKScript.aim = aim; // update state on IK script

		}



	}

	void FixedUpdate () // UPDATES THAT INVOLVES PHYSICS AND ANIMATION
	{
		anim.SetBool ("jump", jump);//updates the animator withe the jump state
		jump = Input.GetButtonDown ("Jump");//listens to "space" button to jump
		anim.SetFloat ("vMove", vMovement); //updates the animator with the vertical arrow value, used for walk and run animations
		anim.SetFloat ("hMove", hMovement); //updates the animator with the vertical arrow value, used for turning or left or right step
		anim.SetBool ("running", run); // //updates the animator with the vertical arrow value, used for running
		anim.SetBool ("aim", aim); // //updates the animator with the vertical arrow value, used for equip bow animation

		///////////////////////////////////////////////////////// JUMP AND CLIMB
		if (jump) {
			
			if (ledge) { //if jump was pressed but there is a ledge in front of the player set jump to false. ledge is controled by the IK script
				jump = false;
				IKScript.SaveLedgeTarget (); // saves the position of the ledge raycast  before climbing
				anim.SetBool ("ledge", true);//plays the climb ledge animation and set ledge to false after 1.0f
				Invoke ("DisableLedge",1.0f);
			}

			else{
				if (Time.time - jumpTime > 2.0f) { // if there is no ledge, and last jump was more than 2.0f ago, add impulse force to the player body
					jumpTime = Time.time;
					playerRigidbody.AddForce (new Vector3 (0, 3.5f, 3.5f), ForceMode.Impulse);
				}
		}
	} 

		///////////////////////////////////////////////////////// PICKING ITEMS
		RaycastHit hit;

		if (Input.GetKeyDown (KeyCode.B)) { // if user press the "B" key, raycasts in front of player
			
			if (Physics.Raycast (transform.position + transform.up * 1.5f + transform.forward * 0.5f, -transform.up, out hit, 10.0f)) {
				
				if (hit.transform.tag == "Energy") { // if ray hits something with tag "Energy", playe pick animation, call coroutine for instantiate particle and untag the hit object
					hit.transform.tag="Untagged";
					anim.SetBool ("pick", true);
					StartCoroutine (DestroyEnergy (hit.transform.gameObject));


				} else if (hit.transform.tag == "Arrow") // if ray hits something with tag "Arros", plays pick animation, call coroutine for picking handling and untag hit object
						{
					    hit.transform.tag = "Untagged";
						anim.SetBool ("pick", true);
						Debug.Log ("hey");
						StartCoroutine (PickArrow (hit.transform.gameObject));
						
					   }

				else if(hit.transform.tag == "Lever"){ // if ray hits something with tag "Lever", call coroutine to handle the lever
					hit.transform.tag = "Untagged";
					StartCoroutine(HandleLever(hit));
				}			
			}		
		}
	}//and of Fixed Update


	/////////////////////////////////////////////////////////
	void DisableLedge(){
		
		anim.SetBool ("ledge",false); //set the ledge bool in the animator to false, stops reloading climb ledge animation
	}

	///////////////////////////////////////////////////////// HANDLE THE LEVER PUSHING AND PULLING
		
	public IEnumerator HandleLever(RaycastHit hitObject){
		
		if (hitObject.transform.GetComponent<LeverControl> ().up == true) { // if public lever variable state is up on the lever script
			
			if (energy >= 1) { // and there is cube energy in the inventory
				energyLevelText.text = energyRoman; //outputs on text box
				anim.SetTrigger ("leverPull");//plays lever pull animation
				yield return new WaitForSeconds (1.3f);
				audioSource.clip = leverPushAudio;//load clip to the audio source and play
				audioSource.Play ();
				hitObject.transform.GetComponent<LeverControl> ().goUp = false;//change lever state after animation finished
				
				energy--;//decreases energy

			} else {
				audioSource.clip = lockedMonumentAudio;//load clip to the audio source and play
				audioSource.Play();
			}
				
		} else {
			
				anim.SetTrigger ("leverPush"); // or do the oposite for push
				yield return new WaitForSeconds (1.3f);
				hitObject.transform.GetComponent<LeverControl> ().goUp = true;
		       }	
		hitObject.transform.tag="Lever";
	}
	///////////////////////////////////////////////////////// HANDLES ENERGY CUBE PICKING

	public IEnumerator DestroyEnergy(GameObject objectToDestroy){
		
		yield return new WaitForSeconds(0.8f);
		objectToDestroy.transform.parent = righthand; // attach cube to the hand of the character
		objectToDestroy.transform.localPosition = Vector3.zero; // with zero distance from the hand

		yield return new WaitForSeconds (0.9f);
		Instantiate (energyParticles, righthand.position, Quaternion.LookRotation (Vector3.up));//instantiate particle after animation in the middle

		yield return new WaitForSeconds (0.2f);
		Destroy (objectToDestroy);//destroy cube and stops picking animation
		anim.SetBool ("pick", false);

		yield return new WaitForSeconds(4.0f);
		audioSource.clip = energySound;//loads yhit clip
		audioSource.Play();//plays the clip

		energy++;//increase the number of energy cubes
		string energyRoman = IntToRoman (energy);//convert to romans
		energyLevelText.text =  energyRoman;//outputs
	}

	///////////////////////////////////////////////////////// HANDLES ARROWS PICKING
	public IEnumerator PickArrow(GameObject objectToPick){ // basically the same logic as the previous coroutine. 

		yield return new WaitForSeconds(0.8f);
		objectToPick.transform.parent = righthand;
		objectToPick.transform.localPosition = Vector3.zero;

		yield return new WaitForSeconds (0.9f);
		anim.SetBool ("pick", false);

		yield return new WaitForSeconds (0.2f);
		anim.SetBool ("pick", false);
		Destroy (objectToPick);
		audioSource.clip = energySound;//load clip to the audio source and play
		audioSource.Play();
		arrow +=3;
		bowControlScript.numberOfArrows = arrow;

	}

	///////////////////////////////////////////////////////// HANDLES DAMAGE HIT
	public void HitDamage (){ //called by the animal AI script
		
		anim.SetTrigger ("hit");// plays the hitSound animation
		energyLifeImage.fillAmount -= 0.20f; // decreases the life energy on 0.2 
		Instantiate (damageParticle,transform.position,Quaternion.LookRotation(Vector3.forward)); //instantiate a hit particle system
		audioSource.clip = hitSound;//load clip to the audio source and play
		audioSource.Play();
	}

	///////////////////////////////////////////////////////// ROMAN NUMBER CONVERSION for numbers between 1 and 9
	public string IntToRoman(int num) {
		

		string[] ones={"","I","II","III","IV","V","VI","VII","VIII","IX", "X", "XI","XII","XIII","XIV","XV"};
		string roman="";        
		roman += ones[num];  //number is the index of the array

		return roman;
	}   

	/////////////////////////////////////////////////////////       TRIGGER HANDLER
	void OnTriggerEnter(Collider col){
		if (col.transform.tag == "EnergyLife") {
			energyCharge = true; // charge the life energy if col is the energy fount
			audioSource.clip = loadingEnergyAudio;//load clip to the audio source and play
			audioSource.Play();
		}
		
		else if (col.transform.tag == "Water") { // if player falls into the water		, sets the camera to stop following and reload the scene.
			cameraScript.follow = false;
			cameraScript.look = false;
			menuManagerScript.LoadScene ();
		}
			
	}

	/////////////////////////////////////////////////////////  TRIGGER HANDLER
	void OnTriggerExit(Collider col){

		if (col.transform.tag == "EnergyLife") // stops charging
			energyCharge = false;


	}
}
